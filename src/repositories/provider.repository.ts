import {DefaultCrudRepository} from '@loopback/repository';
import {Provider, ProviderRelations} from '../models';
import {ProveedorDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ProviderRepository extends DefaultCrudRepository<
  Provider,
  typeof Provider.prototype.nit,
  ProviderRelations
> {
  constructor(
    @inject('datasources.proveedor') dataSource: ProveedorDataSource,
  ) {
    super(Provider, dataSource);
  }
}
