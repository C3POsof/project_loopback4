import {DefaultCrudRepository} from '@loopback/repository';
import {Product, ProductRelations} from '../models';
import {ProductosDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ProductRepository extends DefaultCrudRepository<
  Product,
  typeof Product.prototype.product_code,
  ProductRelations
> {
  constructor(
    @inject('datasources.productos') dataSource: ProductosDataSource,
  ) {
    super(Product, dataSource);
  }
}
