import {Entity, model, property} from '@loopback/repository';

@model()
export class Client extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  surname: string;

  @property({
    type: 'number',
    required: true,
  })
  date_of_birth: number;

  @property({
    type: 'string',
  })
  addres?: string;

  @property({
    type: 'number',
  })
  phone?: number;

  @property({
    type: 'string',
    required: true,
  })
  email: string;


  constructor(data?: Partial<Client>) {
    super(data);
  }
}

export interface ClientRelations {
  // describe navigational properties here
}

export type ClientWithRelations = Client & ClientRelations;
